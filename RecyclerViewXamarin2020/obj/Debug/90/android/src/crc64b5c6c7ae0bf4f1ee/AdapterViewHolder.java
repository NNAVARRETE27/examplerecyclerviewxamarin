package crc64b5c6c7ae0bf4f1ee;


public class AdapterViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("RecyclerViewXamarin2020.Class.AdapterViewHolder, RecyclerViewXamarin2020", AdapterViewHolder.class, __md_methods);
	}


	public AdapterViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == AdapterViewHolder.class)
			mono.android.TypeManager.Activate ("RecyclerViewXamarin2020.Class.AdapterViewHolder, RecyclerViewXamarin2020", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
