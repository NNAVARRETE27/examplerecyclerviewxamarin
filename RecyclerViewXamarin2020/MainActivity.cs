﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Widget;
using System.Collections.Generic;
using AdapterView = RecyclerViewXamarin2020.Class.AdapterView;

namespace RecyclerViewXamarin2020
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity 
    { 
        AdapterView adapter;
        RecyclerView recyclerView1;
        RecyclerView.LayoutManager layoutManager;
        EditText enterNumber;
        TextView textView2;
        Button translateNumber, sendNumberhistory;
        List<itemData> phoneNumbers = new List<itemData>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            enterNumber = FindViewById<EditText>(Resource.Id.enterNumber);
            textView2 = FindViewById<TextView>(Resource.Id.textView2);
            translateNumber = FindViewById<Button>(Resource.Id.sendNumber);
            sendNumberhistory = FindViewById<Button>(Resource.Id.sendNumberhistory);
            recyclerView1 = FindViewById<RecyclerView>(Resource.Id.recyclerView1);
            recyclerView1.HasFixedSize = true;
            layoutManager = new LinearLayoutManager(this);
            recyclerView1.SetLayoutManager(layoutManager);

            adapter = new AdapterView(phoneNumbers);
            recyclerView1.SetAdapter(adapter);

            translateNumber.Click += (sender, e) =>
            {
                if (enterNumber.Text == "")
                {
                    Toast.MakeText(this, "Ingresar número al campo", ToastLength.Short).Show();
                }
                else
                {
                    string translatedNumber;
                    translatedNumber = RecyclerViewXamarin2020.Contador.ToNumber(enterNumber.Text);
                    if (string.IsNullOrWhiteSpace(translatedNumber))
                    {
                        textView2.Text = "";
                    }
                    else
                    {
                        textView2.Text = translatedNumber;
                        phoneNumbers.Add(new itemData() { phoneInsertData = translatedNumber });
                        sendNumberhistory.Enabled = true;
                    }
                }
            };
            sendNumberhistory.Click += (sender, e) =>
            {
                recyclerView1.SetAdapter(adapter);
            };


        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}