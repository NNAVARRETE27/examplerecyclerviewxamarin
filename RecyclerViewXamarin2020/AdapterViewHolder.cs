﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace RecyclerViewXamarin2020.Class
{
    public class AdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView phoneInsert;
        public AdapterViewHolder(View itemView) : base(itemView)
        {
            phoneInsert = itemView.FindViewById<TextView>(Resource.Id.phoneInsert);

        }
    }
}