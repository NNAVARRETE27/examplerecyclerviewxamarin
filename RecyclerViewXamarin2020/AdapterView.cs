﻿using Android.Support.V7.Widget;
using Android.Views;
using System.Collections.Generic;

namespace RecyclerViewXamarin2020.Class
{
    class AdapterView : RecyclerView.Adapter
    {
        private List<itemData> phoneNumbers = new List<itemData>();

        public AdapterView(List<itemData> phoneNumbers)
        {
            this.phoneNumbers = phoneNumbers;
        }
        public override int ItemCount
        {
            get
            {
                return phoneNumbers.Count;
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.From(parent.Context);
            View itemView = inflater.Inflate(Resource.Layout.itemList, parent, false);
            return new AdapterViewHolder(itemView);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            AdapterViewHolder viewHolder = holder as AdapterViewHolder;
            viewHolder.phoneInsert.Text = phoneNumbers[position].phoneInsertData;
        }
    }
}